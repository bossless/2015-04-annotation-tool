import data from 'data';
import BaseView from 'ractive_components/base';
import get from 'utils/ajax/get';
import App from 'utils/core/App';

var app = new App({
	launch: function ( el ) {
		app.el = el;

		app.view = new BaseView({ el: el });

		var urlParams = window.location.search.substring( 1 ).split( '&' );
		var params = {};

		urlParams.forEach( function ( param ) {
			var pair = param.split( '=' );
			params[ pair[0] ] = pair[1];
		});

		app.loadCached( params );

		// http://interactive.guim.co.uk/embed/2015/03/2015-03-annotation-tool/index.html?key=TKTK

		window.addEventListener('resize', function () {
			app.view.set({
				width: window.innerWidth
			});
		});

		app.view.on({});
	},
	loadCached: function(params){
		get( 'http://interactive.guim.co.uk/spreadsheetdata/' + params.key + '.json' ).then( function ( json ) {
			var data = JSON.parse( json );
	
			app.view.set({
				content: data.sheets.content,
				width: window.innerWidth,
			});
			
		});
	}
});

export default app;