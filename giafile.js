var gia = exports;

// The project name and unique identifier
gia.name = '2015-04-annotation-tool';
gia.guid = 'c3d5dfb0-cccb-473f-9ed2-00ce4d46a79e';

// The root of the project, relative to http://interactive.guim.co.uk
gia.path = 'embed/2015/04/2015-04-annotation-tool';

// Randomised port, to prevent clashes
gia.port = 6541;

gia.useCdn = [ 'ractive' ];

// Project type. Used for deployment - don't change this!
gia.type = 'standalone';
